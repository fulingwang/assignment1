**Open file and get application working**

---

## Open the file


1. Make sure you have Visual Studio installed.
2. Open folder Addition.
3. Double click Addition.sln
---

## Get application working


1. Click the **Start** button at the top of the home page or press F5.
2. Enter your answer in the box.
3. Click **Calculate** button at the bottom.
4. Next question will pop up and the result of your previous answer will be displayed on the page.
5. Close the window if you want to stop the quiz.

Licensed under the Fuling License

Testing Testing2