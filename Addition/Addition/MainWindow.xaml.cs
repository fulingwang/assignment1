﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Addition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int UpperBound = 501;
        const int LowerBound = 100;
        readonly Random r = new Random(5);
        readonly Random s = new Random(10);
        int x = 0;
        int y = 0;
        int answer = 0;
        string output = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
            File.AppendAllText("output.txt", $"New run starting at {DateTime.Now:MMM dd, yyyy HH:mm:ss}{Environment.NewLine}");
            x += r.Next(LowerBound, UpperBound);
            y += s.Next(LowerBound, UpperBound);
            answer += answer + x + y;
            Question.Content = x + " + " + y + " = ";
            output += "Question : " + x + " + " + y + " = " + Environment.NewLine;
        }
        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            UserInput.Text = string.Empty;
            string UserText = UserInput.Text;
            int UserAnswer = int.Parse(UserText);
            Result.Content = $" {(UserAnswer == answer ? "Correct" : "Incorrct")} ";
            output += "User's Answer: " + UserInput.Text + " " + "Correct Answer: " + answer + Environment.NewLine;
            answer = 0;
            int x = r.Next(LowerBound, UpperBound);
            int y = s.Next(LowerBound, UpperBound);
            answer += answer + x + y;
            Question.Content = x + " + " + y + " = ";
            File.AppendAllText("output.txt", output);
            output = string.Empty;
            output += "Question : " + x + " + " + y + " = " + Environment.NewLine;
        }
        private void UserInput_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
    }
}
